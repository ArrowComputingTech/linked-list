class LinkedList
  def initialize()
    @index = 0
    @list = []
    @list[@index] = Node.new(@index)
  end

  def increment_index
    @index += 1
  end

  def decrement_index
    @index -= 1
  end

  def append(data)
    self.increment_index
    @list[@index] = Node.new(@index, data)
    @list[@index].value(data)
  end

  def prepend(data)
    (0..(@index-1)).each do |current_index|
      @list[(@index - (current_index))].increment_node()
    end
    self.increment_index
    @list.shift()
    @list.unshift(Node.new(1, data))
    @list.unshift(Node.new(0))
  end

  def size
    pp "List size: #{@index}"
  end

  def head
    pp "First node: #{@list[1].get_data}"
  end

  def tail
    pp "Last node: #{@list[-1].get_data}"
  end

  def at(x)
    (@list[x] != nil and x != 0 and x < @list.count) ? (pp "Node #{x}: #{@list[x].get_data}") : (pp "Node doesn't exist.")
  end

  def pop
    @list.pop()
  end

  def contains?(val)
    contains = false
    (0..(@index-1)).each do |i|
      @list[i].get_data == val ? (contains = true; break) : contains = false
    end
    contains
  end

  def find(val)
    contains_at = nil
    (0..(@index-1)).each do |i|
      @list[i].get_data == val ? (contains_at = i; break) : contains_at = nil
    end
    contains_at != nil ? contains_at : "Does not exist."
  end

  def to_s
    str = ""
    (1..(@index-1)).each do |i|
      str.concat("( #{@list[i].get_data()} ) -> ")
    end
    str.concat("nil")
    pp str
  end

  def insert_at(val, index)
    if (index > 0 and index < @index)
      temp_list = []
      (@index).downto(index) {|i|
        temp_list.append(@list[i].get_data())
        @list.pop()
      }
      @list.append(Node.new(index, val))
      self.increment_index
      (temp_list.count-1).downto(0) do |i|
        @list.append(Node.new((@index-i), temp_list[i]))
      end
    else
      return (pp "Index out of range.")
    end
  end

  def remove_at(index)
    if (index > 0 and index < @index)
      temp_list = []
      (@index).downto(index) {|i|
        temp_list.append(@list[i].get_data())
        @list.pop()
      }
      @list.pop()
      self.decrement_index()
      (temp_list.count-1).downto(0) do |i|
        @list.append(Node.new((@index-i), temp_list[i]))
      end
    else
      return (pp "Index out of range.")
    end
  end
end

class Node
  def initialize(index=nil, data=nil)
    @data = data
    @node_id = index
    node_id = nil ? @next_node_id = 1 : @next_node_id = index + 1
  end

  def value(data)
    @data = data
  end

  def increment_node
    @node_id += 1
    @next_node_id += 1

  end

  def decrement_node
    @node_id -= 1
    @next_node_id -= 1
  end

  def reset_node
    @node_id = 0
    @next_node_id = 1
    @data = nil
  end

  def get_data
    @data
  end
end

ll = LinkedList.new()
ll.append(22)
ll.append("h")
ll.prepend(7)
ll.prepend("Horse")
pp ll
ll.size()
ll.head()
ll.tail()
ll.at(2)
ll.at(5)
pp ll
pp ll.contains?("Horse")
pp ll.contains?(99)
pp ll.find(7)
ll.to_s
ll.insert_at(88, 2)
pp "List:"
pp ll
ll.remove_at(3)
pp ll
